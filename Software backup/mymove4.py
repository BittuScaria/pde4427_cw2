#!/usr/bin/env python

import rospy
import time
from std_msgs.msg import UInt8
from geometry_msgs.msg import Twist


def direction_callback(msg):
	global direction_value
	direction_value = msg.data
	##print(moist_value)		



def mover():
	global direction_value
	global accomplished_id
	direction_value = UInt8()
	accomplished_id = UInt8() 
	move_on1 = 1
	move_on2 = 1
	move_on3 = 1
	move_on4 = 1
	move_on5 = 1
	move_on6 = 1
	move_on7 = 1
	move_on8 = 1
	rospy.init_node('mymove', anonymous=True)
	pub = rospy.Publisher('cmd_vel', Twist, queue_size=10)
	pub2 = rospy.Publisher('task_accomplished', UInt8, queue_size=10)
	global move
	move = Twist()
	rate = rospy.Rate(10)
	sub1 = rospy.Subscriber('move_position', UInt8, direction_callback)
	while not rospy.is_shutdown():
	   
	   if (direction_value == 1
		and move_on1 == 1):	
			move.linear.x = 0.0
			move.angular.z = 0.0
			pub.publish(move)
			time.sleep(2)
			move.linear.x = 0.1
			move.angular.z = 0.0
			pub.publish(move)
			time.sleep(2.2)
			move.linear.x = 0.0
			move.angular.z = 0.0
			pub.publish(move)
			print ('Move Accomplished to direction1')
			accomplished_id.data = 1
			pub2.publish(accomplished_id)
			time.sleep(3)
			move_on1 = (move_on1) + 1
			move_on2 = 1
			move_on3 = 1
			move_on4 = 1
			move_on5 = 1
			move_on6 = 1
			move_on7 = 1
			move_on8 = 1
			rate.sleep()

	   if (direction_value == 2
		and move_on2 == 1):	
			move.linear.x = 0.0
			move.angular.z = 0.0
			pub.publish(move)
			time.sleep(2.0)
			move.linear.x = 0.0
			move.angular.z = -0.4
			pub.publish(move)
			time.sleep(2.1)
			move.linear.x = 0.1
			move.angular.z = 0.0
			pub.publish(move)
			time.sleep(2.75)
			move.linear.x = 0.0
			move.angular.z = 0.0
			pub.publish(move)
			print ('Move Accomplished to direction2')
			move_on2 = (move_on2) + 1
			move_on1 = 1
			move_on3 = 1
			move_on4 = 1
			move_on5 = 1
			move_on6 = 1
			move_on7 = 1
			move_on8 = 1
			rate.sleep()

	   if (direction_value == 3
		and move_on3 == 1):	
			move.linear.x = 0.0
			move.angular.z = 0.0
			pub.publish(move)
			time.sleep(2)
			move.linear.x = 0.0
			move.angular.z = -0.4
			pub.publish(move)
			time.sleep(4.1)
			move.linear.x = 0.1
			move.angular.z = 0.0
			pub.publish(move)
			time.sleep(2.15)
			move.linear.x = 0.0
			move.angular.z = 0.0
			pub.publish(move)
			print ('Move Accomplished to direction 3')
			move_on3 = (move_on3) + 1
			move_on1 = 1
			move_on2 = 1
			move_on4 = 1
			move_on5 = 1
			move_on6 = 1
			move_on7 = 1
			move_on8 = 1
			rate.sleep()


	   if (direction_value == 4
		and move_on4 == 1):	
			move.linear.x = 0.0
			move.angular.z = 0.0
			pub.publish(move)
			time.sleep(2)
			move.linear.x = 0.0
			move.angular.z = -0.4
			pub.publish(move)
			time.sleep(6.2)
			move.linear.x = 0.1
			move.angular.z = 0.0
			pub.publish(move)
			time.sleep(3.0)
			move.linear.x = 0.0
			move.angular.z = 0.0
			pub.publish(move)
			print ('Move Accomplished to direction 4')
			move_on4 = (move_on4) + 1
			move_on1 = 1
			move_on2 = 1
			move_on3 = 1
			move_on5 = 1
			move_on6 = 1
			move_on7 = 1
			move_on8 = 1
			rate.sleep()


	   if (direction_value == 5
		and move_on5 == 1):	
			move.linear.x = 0.0
			move.angular.z = 0.0
			pub.publish(move)
			time.sleep(2)
			move.linear.x = 0.0
			move.angular.z = -0.4
			pub.publish(move)
			time.sleep(8.1)
			move.linear.x = 0.1
			move.angular.z = 0.0
			pub.publish(move)
			time.sleep(2.25)
			move.linear.x = 0.0
			move.angular.z = 0.0
			pub.publish(move)
			print ('Move Accomplished to direction 5')
			move_on5 = (move_on5) + 1
			move_on1 = 1
			move_on2 = 1
			move_on3 = 1
			move_on4 = 1
			move_on6 = 1
			move_on7 = 1
			move_on8 = 1
			rate.sleep()

	   if (direction_value == 6
		and move_on6 == 1):	
			move.linear.x = 0.0
			move.angular.z = 0.0
			pub.publish(move)
			time.sleep(2)
			move.linear.x = 0.0
			move.angular.z = 0.4
			pub.publish(move)
			time.sleep(6.1)
			move.linear.x = 0.1
			move.angular.z = 0.0
			pub.publish(move)
			time.sleep(3.0)
			move.linear.x = 0.0
			move.angular.z = 0.0
			pub.publish(move)
			print ('Move Accomplished to direction 6')
			move_on6 = (move_on6) + 1
			move_on1 = 1
			move_on2 = 1
			move_on3 = 1
			move_on4 = 1
			move_on5 = 1
			move_on7 = 1
			move_on8 = 1
			rate.sleep()


	   if (direction_value == 7
		and move_on7 == 1):	
			move.linear.x = 0.0
			move.angular.z = 0.0
			pub.publish(move)
			time.sleep(2)
			move.linear.x = 0.0
			move.angular.z = 0.4
			pub.publish(move)
			time.sleep(4.0)
			move.linear.x = 0.1
			move.angular.z = 0.0
			pub.publish(move)
			time.sleep(2.15)
			move.linear.x = 0.0
			move.angular.z = 0.0
			pub.publish(move)
			print ('Move Accomplished to direction 7')
			move_on7 = (move_on7) + 1
			move_on1 = 1
			move_on2 = 1
			move_on3 = 1
			move_on4 = 1
			move_on5 = 1
			move_on6 = 1
			move_on8 = 1
			rate.sleep()


	   if (direction_value == 8
		and move_on8 == 1):	
			move.linear.x = 0.0
			move.angular.z = 0.0
			pub.publish(move)
			time.sleep(2)
			move.linear.x = 0.0
			move.angular.z = 0.4
			pub.publish(move)
			time.sleep(2.2)
			move.linear.x = 0.1
			move.angular.z = 0.0
			pub.publish(move)
			time.sleep(2.8)
			move.linear.x = 0.0
			move.angular.z = 0.0
			pub.publish(move)
			print ('Move Accomplished to direction 8')
			move_on8 = (move_on8) + 1
			move_on1 = 1
			move_on2 = 1
			move_on3 = 1
			move_on4 = 1
			move_on5 = 1
			move_on6 = 1
			move_on7 = 1
			rate.sleep()


	   if (direction_value == 9):	
			move_on1 = 1
			move_on2 = 1
			move_on3 = 1
			move_on4 = 1
			move_on5 = 1
			move_on6 = 1
			move_on7 = 1
			move_on8 = 1
	   #else :
			#print('waiting for input')




if __name__ == '__main__':
	try:
		mover()
	except rospy.ROSInterruptException:
		pass

