function AN = getAngles(goalAngle, goalDistance, GRIDSIZE, TOLERANCE)
%calls goalie for each nearest neighbour
%   also normalises the results ready for fuzzy
%   GRIDSIZE is a constant passed into this - hence the caps
angles = [];
for nn=1:8
    [newAngle, newDistance] = goalie(goalAngle,goalDistance*100,nn,GRIDSIZE*100);
    %if the robot is in tolerance then just set the angle to 0
    if (newDistance < TOLERANCE)
        newAngle = 0.0;
    else
        %normalise the angle
        newAngle = abs(newAngle/pi);
    end
    angles = [angles newAngle];
end
    
AN = angles;
end

